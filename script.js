const num1 = Math.ceil(Math.random() * 10);// new
const num2 = Math.ceil(Math.random() * 10);// new
const qE1 = document.getElementById("q");
qE1.innerHTML = 'What is ' + num1 + ' multiply by ' + num2 + '?';
// qE1.innerText='what is ${num1} multiply by ${num2}?'; other way
 // new


const correctAns = num1 * num2;
const formEl = document.getElementById("form");
const inputEl = document.getElementById("input");
let score = JSON.parse(localStorage.getItem("Score")) ;// new
const scoreEl =document.getElementById("score");
if(!score){
    score=0;
}

scoreEl.innerHTML= "Score: " + score;

formEl.addEventListener("submit", () => {
    const userAns = +inputEl.value;// new
    if (correctAns === userAns) {
        score++;
        updateLocalStorage()
        // console.log(score);
    } else {
        score--;
        updateLocalStorage()
    }

})

function updateLocalStorage() {

    localStorage.setItem("Score", JSON.stringify(score));// new
}